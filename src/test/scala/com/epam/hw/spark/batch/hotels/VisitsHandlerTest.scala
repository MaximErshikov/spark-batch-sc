package com.epam.hw.spark.batch.hotels

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpecLike

@org.scalatest.Finders(value = Array("org.scalatest.finders.FlatSpecFinder"))
class VisitsHandlerTest extends AnyFlatSpecLike with BeforeAndAfterAll {

  val spSession = initSparkSession()

  def initSparkSession(): SparkSession = {

    val conf = new SparkConf()
      .setAppName("hotels-stream-handler-test")
      .setMaster("local[*]")

    val session = SparkSession.builder()
      .config(conf)
      .getOrCreate()

    session.sparkContext.setLogLevel("WARN")
    session
  }

  "Idle days calculation" should "produce new column with correct value" in {

    import spSession.implicits._

    val handler = new VisitsHandler()

    val dateOfReference = "2017-09-01"

    val json =
      """{"id":4,
        | "srch_ci":"2017-08-22",
        | "srch_co":"2017-08-23",
        | "hotel_id":970662608899}"""
      .stripMargin

    var df = spSession.read.json(Seq(json).toDS())

    df = handler.calculateIdleDates(df, "idle_days", dateOfReference)

    assert(df.columns.contains("idle_days"))
    assert(9 === df.select("idle_days").first().get(0))
  }

  override def afterAll() {
    spSession.stop()
  }
}
