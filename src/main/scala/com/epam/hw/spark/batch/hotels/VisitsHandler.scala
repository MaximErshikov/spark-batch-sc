package com.epam.hw.spark.batch.hotels

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, datediff, typedLit}

class VisitsHandler {

  def calculateIdleDates(df: DataFrame, colName: String, dateOfReference: String): DataFrame = {

    df.withColumn(colName,
      datediff(typedLit(dateOfReference), col("srch_co")))
  }
}
