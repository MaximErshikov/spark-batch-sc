package com.epam.hw.spark.batch.hotels

import org.apache.spark.SparkConf
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions.{col, count, from_json, year}
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}

import scala.util.Properties.envOrElse

object BatchProcessing {

  private val HDFS_URL = env("HDFS_URL", "hdfs://192.168.121.232:9000")
  private val SPARK_MASTER_URL = env("SPARK_MASTER_URL" ,"local[*]")
  private val KAFKA_BOOTSTRAP_SERVERS = env("KAFKA_BOOTSTRAP_SERVERS","localhost:9092")

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setAppName("hotels-batching")
      .setMaster(SPARK_MASTER_URL)

    val session = SparkSession
      .builder()
      .config(conf)
      .getOrCreate()

    println("SPARK_MASTER_URL: " + SPARK_MASTER_URL)
    println("HDFS_URL: " + HDFS_URL)
    println("KAFKA_BOOTSTRAP_SERVERS: " + KAFKA_BOOTSTRAP_SERVERS)

    println("Idle hotels processing started")
    session.sparkContext.setLogLevel("WARN")

    val dfHotelKafka = session.read
      .format("kafka")
      .option("kafka.bootstrap.servers", KAFKA_BOOTSTRAP_SERVERS)
      .option("subscribe", "hotels")
      .option("startingOffsets", "{\"hotels\":{\"0\":-2}}")
      .option("maxOffsetsPerTrigger", "100")
      .load()

    val dfHotel = dfHotelKafka
      .select(from_json(col("value").cast("string"), hotelSchema())
        .as("rec"))
      .select("rec.*")
      .withColumn("Id", col("Id")
        .cast(LongType))

    println("")
    println("Hotel count: " + dfHotel.count())
    println("Hotel columns: " + dfHotel.columns.mkString("Array(", ", ", ")"))
    print("Hotel schema: ")
    dfHotel.printSchema()


    val dfVisits = session.read
      .format("avro")
      .load(HDFS_URL + "/hw-hdfs/expedia/*")

    println("Visits count: " + dfVisits.count())
    print("Visits schema: ")
    dfVisits.printSchema()

    val dateOfReference = "2017-11-25"
    val visitsHandler = new VisitsHandler()

    val dfVisitsEnriched = visitsHandler
      .calculateIdleDates(dfVisits, "idle_days", dateOfReference)

    val dfInvalidVisits = dfVisitsEnriched
      .filter(col("idle_days") > 1 && col("idle_days") < 30)

    val dfValidVisits = dfVisitsEnriched.except(dfInvalidVisits)

    val dfValidHotelVisits = dfValidVisits.join(dfHotel,
      dfValidVisits("hotel_id") === dfHotel("Id"), "inner")

    val dfInvalidHotelVisits = dfInvalidVisits.join(dfHotel,
      dfValidVisits("hotel_id") === dfHotel("Id"), "inner")


    dfValidVisits
      .withColumn("chin_year", year(col("srch_ci")))
      .write
      .mode(SaveMode.Overwrite)
      .partitionBy("chin_year")
      .format("avro")
      .save(HDFS_URL + "/hw-spark/expedia/")


    val dfWriteResult = session.read
      .format("avro")
      .load(HDFS_URL + "/hw-spark/expedia/*")

    println("Written visits count: " + dfWriteResult.count())
    dfWriteResult.printSchema()

    println("Invalid Hotels:")
    dfInvalidHotelVisits
      .select("Name", "City", "Address")
      .foreach(row => println(row))


    println("")
    println("Hotel visits by Country:")
    dfValidHotelVisits
      .select("Country")
      .groupBy("Country")
      .agg(count("Country").alias("count"))
      .foreach(row => println(row.getString(0) + " " + row.getLong(1)))


    println("")
    println("Writing Hotel visits by City into HDFS...")
    val visitsByCityPath = "/hw-spark/visits-by-city.csv"
    dfValidHotelVisits
      .select("City")
      .groupBy("City")
      .agg(count("City").alias("count"))
      .repartition(1)
      .write
      .mode(SaveMode.Overwrite)
      .csv(HDFS_URL + visitsByCityPath)
//      .foreach(row => println(row.getString(0) + " " + row.getLong(1)))
    println("Finished: Writing Hotel visits by City into HDFS: " + visitsByCityPath)

    println("")
    println("Invalid visits: " + dfInvalidVisits.count())
    println("Valid visits: " + dfValidVisits.count())
    println("Joined visits: " + dfValidHotelVisits.count())

    session.stop()
  }

  def hotelSchema(): StructType = {
    StructType(Array(
        StructField("Id", StringType, nullable = true),
        StructField("Name", StringType, nullable = true),
        StructField("Country", StringType, nullable = true),
        StructField("City", StringType, nullable = true),
        StructField("Address", StringType, nullable = true),
        StructField("Latitude", StringType, nullable = true),
        StructField("Longitude", StringType, nullable = true),
        StructField("GeoHash", StringType, nullable = true)))
  }

  def env(name: String, defaultValue: String): String = {
    envOrElse(name, defaultValue)
  }
}
